<?php

use yii\db\Migration;

/**
 * Class m200115_164823_add_article_table
 */
class m200115_164823_add_article_table extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%article}}', [
                'id' => $this->primaryKey(),
                'title'=>$this->string(),
                'description'=>$this->text(),
                'content'=>$this->text(),
                'image'=>$this->string(),
                'user_id'=>$this->integer(),
                'status'=>$this->integer(),
                'created_at' => $this->datetime()->notNull(),
                'updated_at' => $this->datetime()->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%article}}');
    }
}
