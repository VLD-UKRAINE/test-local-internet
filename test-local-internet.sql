-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u8
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Янв 20 2020 г., 19:28
-- Версия сервера: 5.5.62-0+deb8u1
-- Версия PHP: 5.6.40-0+deb8u8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `test-local-internet`
--

-- --------------------------------------------------------

--
-- Структура таблицы `article`
--

CREATE TABLE IF NOT EXISTS `article` (
`id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `content` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `article`
--

INSERT INTO `article` (`id`, `title`, `description`, `content`, `image`, `user_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Изучаем регулярные выражения', 'Научитесь пользоваться самыми эффективными средствами из всех доступных!!', 'В этом кратком справочнике представлены самые важные сведения о регулярных выражениях. В книге поэтапно описана процедура составления и проверки регулярных выражений, а основные понятия подробно поясняются и подкрепляются наглядными примерами, взятыми из практики обработки исходного текста. В конце книги приведены решения часто встречающихся практических задач с помощью регулярных выражений, а также кратко описаны их основные реализации в языках программирования и приложениях.\r\n\r\nКнига написана простым и доступным языком. Она будет полезна не только начинающим, но и тем, кто уже пользовался регулярными выражениями в своей практике прикладного программирования.\r\n\r\nЗнатоки регулярных выражений уже давно включили их в свой арсенал средств, чтобы выполнять самые разные, изощренные виды обработки текста и манипулирования практически на любом языке программирования и на любой вычислительной платформе. Но это лишь одна, приятная, сторона дела. А обратная, неприятная, сторона состоит в том, что регулярные выражения слишком долго оставались исключительным средством только для самых технически грамотных пользователей. Но так было до сих пор.\r\n\r\nЭта книга поможет вам научиться пользоваться теми регулярными выражениями, которые действительно нужно знать, начиная с поиска простых совпадений с заданным текстом и заканчивая более сложными задачами, включая применение обратных ссылок, условные вычисления и обработку с упреждением.\r\n\r\nПрорабатывая материал каждого урока в данной книге, вы методически, систематически и легко научитесь решать практические задачи, используя регулярные выражения.\r\n\r\nРегулярные выражения не так сложны, как кажется. Чтобы умело ими пользоваться, достаточно уяснить поставленную задачу и ее наилучшее решение с помощью регулярных выражений.', '0b6abcab6471811965e28e5ba65fb1f1.jpg', 1, 1, '2020-01-17 21:21:04', '2020-01-20 17:24:45'),
(2, 'Шаблоны корпоративных приложений (Signature Series)', 'Книга «Шаблоны корпоративных приложений» является переизданием книги «Архитектура корпоративных программных приложений» !', 'Создание компьютерных систем — дело далеко не простое. По мере того как возрастает их сложность, процессы конструирования соответствующего программного обеспечения становятся все более трудоемкими, причем затраты труда растут экспоненциально. Как и в любой профессии, прогресс в программировании достигается исключительно путем обучения, причем не только на ошибках, но и на удачах — как своих, так и чужих. Книга дает ответы на трудные вопросы, с которыми приходится сталкиваться всем разработчикам корпоративных систем. Автор, известный специалист в области объектно-ориентированного программирования, заметил, что с развитием технологий базовые принципы проектирования и решения общих проблем остаются неизменными, и выделил более 40 наиболее употребительных подходов, оформив их в виде типовых решений. Результат перед вами — незаменимое руководство по архитектуре программных систем для любой корпоративной платформы. Это своеобразное учебное пособие поможет вам не только усвоить информацию, но и передать полученные знания окружающим значительно быстрее и эффективнее, чем это удавалось автору. Книга предназначена для программистов, проектировщиков и архитекторов, которые занимаются созданием корпоративных приложений и стремятся повысить качество принимаемых стратегических решений.\r\n" Домашняя страница Мартина Фаулера "', NULL, 1, 1, '2020-01-18 17:21:17', '2020-01-18 17:21:17'),
(3, 'Необходимо разработать блог с поиском на Sphinx!', 'Должны быть страницы:\r\nРегистрация\r\nАвторизация\r\nCRUD операции для добавления статьи на блог. То есть после авторизации, человек может добавить статью на блог, удалить её или редактировать.\r\n', 'Поля для добавления статьи:\r\nЗаголовок статьи\r\nКраткое описание статьи\r\nПолное описание статьи\r\nКартинка. При загрузке картинки, она должна адаптивно урезаться под размеры 640 x 480. Если картинка изначально меньше этих размеров, то запрещает такую загружать.\r\nКнопка “Сохранить”\r\n', 'a73ec443392d6617a97bfd3377489caf.jpg', 1, 1, '2020-01-19 21:54:11', '2020-01-20 17:45:18'),
(4, 'Test2', 'ewrwerewrefwef', 'wewefwefwefw', NULL, 1, 1, '2020-01-20 11:23:02', '2020-01-20 11:23:02'),
(6, 'Предисловие', 'Весной 1999 года меня пригласили в Чикаго для консультаций по одному из проектов,\r\n', 'осуществляемых силами ThoughtWorks — небольшой, но быстро развивавшейся компании, которая занималась разработкой программного обеспечения. Проект был достаточно амбициозен: речь шла о создании корпоративного лизингового приложения уровня\r\nсервера, которое должно было охватывать все аспекты проблем имущественного найма,\r\nвозникающих после заключения договора: рассылку счетов, изменение условий аренды,\r\nпредъявление санкций нанимателю, не внесшему плату в установленный срок, досрочное возвращение имущества и т.п. Все это могло бы звучать не так уж плохо, если не задумываться над тем, сколь разнообразны и сложны формы соглашений аренды. "Логика"\r\nбизнеса редко бывает последовательна и стройна, так как создается деловыми людьми\r\nдля ситуаций, в которых какой-нибудь мелкий случайный фактор способен обусловить\r\nогромные различия в качестве сделки — от полного краха до неоспоримой победы.\r\nЭто как раз те вещи, которые интересовали меня прежде и не перестают волн', 'b20f8ab21577f8ad5f25a3c70eee8488.jpg', 1, 1, '2020-01-20 18:59:56', '2020-01-20 19:11:47');

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1579288790),
('m130524_201442_init', 1579288794),
('m190124_110200_add_verification_token_column_to_user_table', 1579288794),
('m200115_164823_add_article_table', 1579288794);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `verification_token`) VALUES
(1, 'VLAD', 'JDt1Ps33LAfhI3IIzmrGdV2ZoSBc-mFB', '$2y$13$6Pi5rCWu1FMdUZVV78n2H.ayypaHUW7T9VSoANhbEPSBqTJTv/KpK', NULL, 'vlad555vlad@gmail.com', 10, '2020-01-17 21:20:43', '2020-01-17 21:20:43', 'qFX8FQ4LMvw47whQWn6Rocku5bVWZN_y_1579288841');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `article`
--
ALTER TABLE `article`
 ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
 ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `username` (`username`), ADD UNIQUE KEY `email` (`email`), ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `article`
--
ALTER TABLE `article`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
