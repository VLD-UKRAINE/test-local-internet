<?php

namespace common\models;

use Yii;

/**
 * This is the model class for index "inx_article".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $content
 * @property string $image
 * @property string $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $user_id
 */
class ArticleSphinx extends \yii\sphinx\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function indexName()
    {
        return 'inx_article';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'unique'],
            [['id', 'user_id'], 'integer'],
            [['title', 'description', 'content', 'image', 'status', 'created_at', 'updated_at'], 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'content' => 'Content',
            'image' => 'Image',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'user_id' => 'User ID',
        ];
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return ($this->image) ? '/uploads/' . $this->image : '/uploads/no-image.png';
    }
}
