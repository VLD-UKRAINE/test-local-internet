<?php
namespace common\models;


use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\imagine\Image;
use Imagine\Gd;
use Imagine\Image\Box;
use Imagine\Image\BoxInterface;

class ImageUpload extends Model

{
public $image;


    public function rules()
    {
        return [
            [['image'], 'required'],
            [['image'], 'file', 'extensions' => ['jpg','png']],
            ['image', 'image', 'minWidth' => 640, 'minHeight' => 480]
        ];
    }

    /**
     * @param UploadedFile $file
     * @param $currentImage
     * @return string
     */
    public function uploadFile(UploadedFile $file, $currentImage)
    {
        $this->image = $file;

        if($this->validate())
        {
            $this->deleteCurrentImage($currentImage);
            return $this->saveImage();
        }
    }


    /**
     * @return string
     */
    public function getFolder()
    {
        return Yii::$app->basePath . '\web\uploads'.'\\';
    }

    /**
     * @param $currentImage
     */
    public function resizeImage($savedImage){
        $newWidth = 640;
        $newHeight = 480;
        Image::getImagine()->open($savedImage)->thumbnail(new Box($newWidth, $newHeight))->save($savedImage, ['quality' => 90]);
        //TODO Найти что то для ресайза картинки при загрузке
    }

    /**
     * @return string
     */
    public function generateFilename()
    {
        return strtolower(md5(uniqid($this->image->baseName)) . '.' . $this->image->extension);
    }


    /**
     * @param $currentImage
     */
    public function deleteCurrentImage($currentImage)
    {
        if($this->fileExists($currentImage))
        {
            unlink($this->getFolder() . $currentImage);
        }
    }


    /**
     * @param $currentImage
     * @return bool
     */
    public function fileExists($currentImage)
    {
        if(!empty($currentImage) && $currentImage != null)
        {
            return file_exists($this->getFolder() . $currentImage);
        }
    }


    /**
     * @return string
     */
    public function saveImage()
    {
        $filename = $this->generateFilename();
        $this->image->saveAs($this->getFolder() . $filename);
        $this->resizeImage($this->getFolder() . $filename);
        return $filename;
    }


}