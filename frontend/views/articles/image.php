<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\fileinput;

/* @var $this yii\web\View */
/* @var $model common\models\Image */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Image upload';
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';

?>

<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>
    <?php echo $form->field($model, 'image')->widget(FileInput::classname(), [
        'options' => [
            'accept' => 'image/*',
        ],
        'pluginOptions' => [
            'allowedFileExtensions'=> ["jpg", "png", "gif"],
            'showPreview' => true,
            'minImageWidth'=> 640,
            'minImageHeight' => 480,
            'maxFileCount' => 1,
            ],

    ]);?>
    <?php ActiveForm::end(); ?>

</div>




