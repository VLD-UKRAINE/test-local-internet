<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Articles';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-index">


    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Article', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options'=>['style'=>'white-space: normal;'],

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'title',
            [
                'format' => 'html',
                'attribute' => 'title',
                'label' => 'Title',
                'value' => function($data){
                    return Html::a($data->title, Url::to(['articles/view', 'id'=>$data->id]));
                }
            ],
            'description:ntext',
            'image',
            [
                'format' => 'html',
                'label' => 'Photo',
                'value' => function($data){
                       return Html::img($data->getImage(), ['width'=>200]);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
