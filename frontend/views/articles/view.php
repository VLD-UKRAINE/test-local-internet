<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Article */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Articles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="article-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Upload Image', ['upload-image', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'description:ntext',
            'content:ntext',
            'image',
            [
                'format' => 'html',
                'label' => 'Photo',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>200]);
                }
            ],
//            'user_id',
//            'status',
            'created_at',
            'updated_at',
        ],
    ]) ?>

    <div class="ya-share2"
         data-services="collections,vkontakte,odnoklassniki,facebook"
         data-limit="3"
         data-url ="<?= Url::to('/articles/view/'.$model->id, 'http' )?>"
    ></div>

</div>
